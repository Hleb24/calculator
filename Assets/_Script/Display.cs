﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum State
{
    idle,
    run,
    result
}

public class Display : MonoBehaviour
{
    
    // Start is called before the first frame update
    static public Display D;
    [Header("Set Dynamically")]
    public Text displayText;
    public State state;
    

    // Start is called before the first frame update
    void Awake()
    {
        if (D == null)
            D = this;
        GameObject display = GameObject.Find("Display");
        displayText = display.GetComponent<Text>();
        State state = State.idle;
    }

    // Update is called once per frame
    void Update()
    {
        if (D.displayText.text != "0" && state != State.result)
            state = State.run;
        
    }
    public void SetRun ()
    {
        if (state == State.idle)
            state = State.run;
    }
    public void SetResult()
    {
        if (state == State.run)
            state = State.result;
    }
    public void SetIdle()
    {
        state = State.idle;
    }
}
