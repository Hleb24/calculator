﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Data;
using System.Linq;

public class Operations : MonoBehaviour
{
    public Text result;
    public Text sqRoot;
    private string[] operationsSymbols = { "+", "-", "*", "/" };

    public void OnClickSqRoot()
    {
        Display.D.displayText.text = SqRoot(result.text);
        Display.D.SetResult();
    }
    public void OnClickPower()
    {
        Display.D.displayText.text = Power(result.text);
        Display.D.SetResult();
    }
    public void Result()
    {
        Display.D.displayText.text=Calculate(result.text);
        Display.D.SetResult();
    }
    public void Discard()
    {
        Display.D.displayText.text = "0";
        Display.D.SetIdle();
    }
    public string SqRoot(string tepmSq)
    {
        float value = 0;
        if (EquotionHasOperation(tepmSq))
        {
            value = float.Parse((new DataTable().Compute(tepmSq, "")).ToString());
        }
        else
        {
            value = float.Parse(tepmSq);
        }

        var result = Mathf.Pow(value, 0.5f);
        return result.ToString();
    }
    public string Power(string tepmSq)
    {
        float value = 0;
        if (EquotionHasOperation(tepmSq))
        {
            value = (float)(new DataTable().Compute(tepmSq, ""));
        }
        else
        {
            value = float.Parse(tepmSq);
        }

        var result = Mathf.Pow(value, 2f);
        return result.ToString();
    }
    public string Calculate(string tempR)
    {
        string result = (new DataTable().Compute(tempR, "")).ToString();
        return result;
    }

    public bool EquotionHasOperation(string equation)
    {
        return operationsSymbols.Any(x => equation.Contains(x));
    }
    
}
