﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Butt : MonoBehaviour
{   
    public  Text _buttText;
    private string[] operationsSymbols = {"+", "-", "*", "/"};

    public void OnClickNumber()
    {
        if (Display.D.state == State.idle)
        {
            Display.D.displayText.text = _buttText.text;
            return;
        }
        if (Display.D.state == State.result)
        {
            Display.D.SetIdle();
        }
        
        
        Display.D.SetRun();
        if (Display.D.state == State.run)
        {
            Display.D.displayText.text += _buttText.text;
        }
    }
    public void OnClickOperation()
    {        
        if (Display.D.state == State.idle)
            return;

        var lastChar = Display.D.displayText.text.Substring(Display.D.displayText.text.Length - 1, 1);

        if (operationsSymbols.Contains(lastChar)) return;

        Display.D.displayText.text += _buttText.text;
    }
    
}
